package com.demo;

import com.demo.entity.Consumer;
import com.demo.entity.Person;
import com.demo.es.mapper.ConsumerMapper;
import com.demo.es.mapper.PersonMapper;
import lombok.extern.slf4j.Slf4j;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @Project: easy-es-demo
 * explain: 文档增删改操作
 */
@Slf4j
@SpringBootTest
public class DocumentCrudTest {

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ConsumerMapper consumerMapper;

    /**
     * 单条新增
     */
    @Test
    void insert(){

        Person person = new Person();

        person.setName("叶凡");
        person.setAddress("遮天.");
        person.setAge(20);

        // 进行添加操作 添加成功后会将id返回
        Integer i = personMapper.insert(person);

        log.info(i > 0 ? "添加成功!" : "添加失败!");
        log.info(i > 0 ? "id: " + person.getId() : "");
    }

    /**
     * 单条新增2
     */
    @Test
    void insert2(){
        Consumer consumer = new Consumer("慕雨墨", 22,
                "慕雨墨，来自杀手组织暗河，慕家家主，实力为逍遥天境。 [2] 人物经历. 播报. 年轻的时候人称“蜘蛛女");
        Integer i = consumerMapper.insert(consumer);

        log.info(i > 0 ? "添加成功!" : "添加失败!");
        log.info(i > 0 ? "id: " + consumer.getId() : "");
    }


    /**
     * 批量新增
     */
    @Test
    void bathInsert(){
        Consumer one =
                new Consumer("苏暮雨", 20, "暗河苏家的家主，前代暗河大家长直属蛛影杀手团的首领“傀”，人称“执伞鬼”。他曾经与前任魔教教主叶鼎之、前任北离大监浊清、怒剑仙颜战天并称为天下四大魔头.");

        Consumer two =
                new Consumer("苏昌河", 20, "暗河苏家的大家长.");

        Consumer three =
                new Consumer("旺财", 22, "在数字化时代，人们对于高效、快速的内容创作需求日益增加，而人工智能（AI）作为技术的一大突破，带来了许多改变和便利");

        Consumer four =
                new Consumer("富贵", 21, "合多项影响力指标，我们从上半年收录的项目中挑选出20条优质文案。 其中，有些如诗般浓缩对生活的观察与感受，有些寥寥数语但字字铿锵有力.");

        List<Consumer> consumers = Arrays.asList(one, two, three, four);

        // 进行批量添加 添加成功后会返回 ID
        Integer batch = consumerMapper.insertBatch(consumers);
        if (batch > 0) {
            log.info("批量添加成功 , 他们的ID为:");
            consumers.forEach(consumer -> System.out.println(consumer.getId()));
        }else {
            log.info("批量添加失败!");
        }
    }

    /**
     * 根据id删除元素
     */
    @Test
    void deleteItem(){
        Integer i = personMapper.deleteById("fd6017d6-213c-43dd-9e4c-58a217b238e6");

        log.info(i > 0 ? "删除成功!" : "删除失败!");
    }

    /**
     * 根据条件进行删除
     */
    @Test
    void conditionDelete(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        // name为虎子的数据都会删除
        wrapper.eq(Consumer::getName, "虎子");

        // 进行删除
        Integer i = consumerMapper.delete(wrapper);

        log.info(i > 0 ? "删除成功, 删除数量: " + i : "删除失败!");
    }


    /**
     * 批量删除
     */
    @Test
    void batchDelete(){
        List<String> ids = Arrays.asList("uXoXFYsBLB7Xkj4MQknf", "u3oYFYsBLB7Xkj4MbUnQ");
        Integer i = personMapper.deleteBatchIds(ids);
        log.info(i > 0 ? "删除成功, 删除数量: " + i : "删除失败!");
    }

    /**
     * 根据Id进行修改
     */
    @Test
    void updateById(){

        Person person = new Person();
        person.setId("00d39e29-f095-4b33-a450-4bb020df69d3");
        person.setAddress("遮天-叶天帝");

        // 进行修改
        Integer i = personMapper.updateById(person);
        log.info(i > 0 ? "修改成功!" : "修改失败!");
    }

    /**
     * 根据自定义条件进行修改
     */
    @Test
    void update(){

        Consumer consumer = new Consumer();
        consumer.setAge(14);

        // 封装条件
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<Consumer>().eq(Consumer::getName, "旺财");

        // 进行i需改
        Integer i = consumerMapper.update(consumer, wrapper);
        log.info(i > 0 ? "修改成功, 修改数量: " + i : "修改失败!");
    }

    /**
     * 根据Id进行批量更新
     */
    @Test
    void batchUpdate(){

        Consumer consumer = new Consumer();
        consumer.setName("小富贵");
        consumer.setId("OOZnNosBajdAeR-iq-Df");

        Consumer consumerTwo = new Consumer();
        consumerTwo.setName("小旺财");
        consumerTwo.setId("N-ZnNosBajdAeR-iq-Df");

        List<Consumer> consumers = Arrays.asList(consumer, consumerTwo);
        // 进行修改
        Integer i = consumerMapper.updateBatchByIds(consumers);

        log.info(i > 0 ? "批量修改成功, 修改数量: " + i : "批量修改失败!");
    }

}
