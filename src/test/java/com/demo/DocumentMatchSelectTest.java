package com.demo;

import com.demo.entity.Consumer;
import com.demo.es.mapper.ConsumerMapper;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 
 * @Project: easy-es-demo
 * explain: 文档分词查询
 */
@SpringBootTest
public class DocumentMatchSelectTest {

    @Autowired
    private ConsumerMapper consumerMapper;

    /**
     * 分词查询
     */
    @Test
    void testMatch(){

        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();
        // 分词条件 会将 '苏暮雨' 三个字进行分词 分成 苏 暮 雨; Name中包含这三个字其中一个就会被搜索出来
        wrapper.match(Consumer::getName, "苏暮雨");

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 分词查询 完全包含指定的条件才会被查询出
     */
    @Test
    void containMatch(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        // 完全包含 苏暮雨 才会被查出
        wrapper.matchPhrase(Consumer::getName, "苏暮雨");

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 查询全部数据
     */
    @Test
    void matchAll(){
        List<Consumer> consumers = consumerMapper.selectList(new LambdaEsQueryWrapper<Consumer>().matchAllQuery());

        consumers.forEach(System.out::println);
    }

    /**
     * 前缀查询
     */
    @Test
    void MatchPhrasePrefixQuery(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.matchPhrasePrefixQuery(Consumer::getName, "苏暮雨", 10);

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 从多个字段中查询包含指定文字的数据
     */
    @Test
    void testMultiMatchQuery(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        // name跟remark中包含暗河就给查询出来
        wrapper.multiMatchQuery("暗河", Consumer::getName, Consumer::getRemark);

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 从所有字段中查询包含指定关键字的数据
     */
    @Test
    void queryStringQuery(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<Consumer>()
                .queryStringQuery("暗河");

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

}
