package com.demo;

import com.demo.entity.Consumer;
import com.demo.entity.Person;
import com.demo.es.mapper.ConsumerMapper;
import com.demo.es.mapper.PersonMapper;
import lombok.extern.slf4j.Slf4j;
import org.dromara.easyes.annotation.rely.FieldType;
import org.dromara.easyes.core.conditions.index.LambdaEsIndexWrapper;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 
 * @Project: easy-es-demo
 * explain: 使用easy-es操作elasticsearch示例
 */
@Slf4j
@SpringBootTest
public class EasyEsTest {

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ConsumerMapper consumerMapper;

    @Test // 创建索引
    void crateIndex(){
        // 创建名为consumer的索引
        personMapper.createIndex("consumer");
    }

    /**
     * 创建索引并设置字段的映射类型
     */
    @Test
    void createIndex2(){
        // 创建 索引条件 对象
        LambdaEsIndexWrapper<Consumer> indexWrapper = new LambdaEsIndexWrapper<>();

        // 此处简单起见 索引名称须保持和实体类名称一致,字母小写 后面章节会教大家更如何灵活配置和使用索引
        indexWrapper.indexName(Consumer.class.getSimpleName().toLowerCase());

        // 第二个参数表示: 参数的类型 keyword
        indexWrapper.mapping(Consumer::getName, FieldType.KEYWORD);
        indexWrapper.mapping(Consumer::getAge, FieldType.INTEGER);

        // keyword字符   text类型为可分词类型
        indexWrapper.mapping(Consumer::getRemark, FieldType.KEYWORD);

        // 设置 索引分词 查询分词 可以指定不同的分词器 如果没有指定则使用 ES 默认分词器 IK_SMART 使用的是ik分词器 需要在es插件中安装 否则无法使用.
//        indexWrapper.mapping(Consumer::getRemark, FieldType.TEXT, Analyzer.IK_SMART, Analyzer.IK_MAX_WORD);

        // 创建索引
        Boolean b = consumerMapper.createIndex(indexWrapper);

        log.info(b ? "索引创建成功!" : "索引创建失败!");
    }

    /**
     * 删除索引
     */
    @Test
    void deleteIndex(){
        // 根据索引名删除索引
        Boolean deleteIndex = consumerMapper.deleteIndex("consumer");
        log.info(deleteIndex ? "索引删除成功!" : "索引删除失败!");
    }

    /**
     * 新增一个数据
     */
    @Test
    void insert(){
        Person person = new Person();
        person.setAddress("邯郸");
        person.setAge(23);
        person.setName("苏暮雨");
        // 会返回新增的数量 添加成功后会返回 ID 到person的id属性中
        Integer i = personMapper.insert(person);
        log.info(i > 0 ? "添加成功, 他的ID为:" + person.getId() : "添加失败!");
    }

    /**
     * 根据id删除元素
     */
    @Test
    void deleteItem(){
        Integer i = personMapper.deleteById("fd6017d6-213c-43dd-9e4c-58a217b238e6");
        log.info(i > 0 ? "删除成功!" : "删除失败!");
    }



    // 查询全部数据
    @Test
    void findAll(){

        // matchAll查询全部数据
        List<Consumer> consumers = consumerMapper.selectList(new LambdaEsQueryWrapper<Consumer>().matchAllQuery());

        consumers.forEach(System.out::println);
    }

    /**
     * 根据Id进行查询
     */
    @Test
    void findById(){
        Consumer consumer = consumerMapper.selectById("wHogJ4sBLB7Xkj4MTEnB");
        System.out.println(consumer.toString());
    }

    /**
     * 条件查询
     */
    @Test
    void findEq(){
        // 封装条件
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        wrapper.eq(Consumer::getName, "苏昌河");

        // 进行查询
        Consumer consumer = consumerMapper.selectOne(wrapper);

        System.out.println(consumer);
    }

    /**
     * 模糊查询
     */
    @Test
    void findLike(){
        // 封装条件
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        // like *词汇*
//        wrapper.like(Consumer::getName, "苏");
        // likeRight 词汇* 以苏字开头
//        wrapper.likeRight(Consumer::getName, "苏");
        // likeLeft *词汇 以河字结尾
        wrapper.likeLeft(Consumer::getName, "河");

        // 查询
        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

}
