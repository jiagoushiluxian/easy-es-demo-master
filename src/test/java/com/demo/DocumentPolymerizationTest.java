package com.demo;

import com.demo.entity.Consumer;
import com.demo.es.mapper.ConsumerMapper;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 
 * 聚合查询
 */
@SpringBootTest
public class DocumentPolymerizationTest {

    @Autowired
    private ConsumerMapper consumerMapper;

    /**
     * 去重查询
     */
    @Test
    void findDistinct(){
        List<Consumer> consumers = consumerMapper.selectList(
                new LambdaEsQueryWrapper<Consumer>()
                        .distinct(Consumer::getAge) // 将age重复的列进行去除
        );
        consumers.forEach(System.out::println);
    }

    /**
     * 聚合查询 查询最大的年龄
     */
    @Test
    void findMaxAge(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<Consumer>()
                .max(Consumer::getAge);

        SearchResponse res = consumerMapper.search(wrapper);

        Aggregations aggregations = res.getAggregations();

        for (Aggregation aggregation : aggregations) {
            String type = aggregation.getType();
            String name = aggregation.getName();
            System.out.println(name);
            System.out.println(type);
        }
    }

}
