package com.demo;

import com.demo.entity.Consumer;
import com.demo.entity.User;
import com.demo.es.mapper.ConsumerMapper;
import com.demo.es.mapper.UserMapper;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.UUID;

/**
 * 
 * 高亮查询
 */
@SpringBootTest
public class DocumentHighLightTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    void insert(){
        Integer i = userMapper.insert(new User().setName("李白").setAddress("他出生在中国,河北省,石家庄市."));
        System.out.println(i);
    }

    /**
     * 高亮查询 根据提供的关键词 进行查询数据 并且查询出的数据会根据自定义标签包裹
     */
    @Test
    void highlight(){

        LambdaEsQueryWrapper<User> wrapper = new LambdaEsQueryWrapper<>();

        // address列中包含 中国 二字
        wrapper.match(User::getAddress, "中国");

        List<User> users = userMapper.selectList(wrapper);

        users.forEach(System.out::println);
    }

}
