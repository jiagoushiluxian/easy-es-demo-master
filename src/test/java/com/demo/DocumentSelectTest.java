package com.demo;

import com.demo.entity.Consumer;
import com.demo.es.mapper.ConsumerMapper;
import lombok.extern.slf4j.Slf4j;
import org.dromara.easyes.core.biz.EsPageInfo;
import org.dromara.easyes.core.biz.SAPageInfo;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.dromara.easyes.core.core.EsWrappers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @Project: easy-es-demo
 * explain: 文档查询测试案例
 */
@Slf4j
@SpringBootTest
public class DocumentSelectTest {

    @Autowired
    private ConsumerMapper consumerMapper;

    /**
     * 查询全部数据 matchAll
     */
    @Test
    void findAll(){
        List<Consumer> consumers = consumerMapper.selectList(new LambdaEsQueryWrapper<Consumer>().matchAllQuery());
        consumers.forEach(System.out::println);
    }

    /**
     * 根据Id进行查询
     */
    @Test
    void findById(){
        Consumer consumer = consumerMapper.selectById("wHogJ4sBLB7Xkj4MTEnB");
        System.out.println(consumer.toString());
    }

    /**
     * 浅分页查询 物理分页 适用于查询数据量少于1万的情况
     */
    @Test
    void findPage(){

        // 查询条件 matchAll 查询全部
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<Consumer>().matchAllQuery();

        // 分页条件
        EsPageInfo<Consumer> pageQuery = consumerMapper.pageQuery(wrapper,2, 5);
        System.out.println(pageQuery);
    }

    /**
     * 条件查询
     */
    @Test
    void findEq(){
        // 封装条件
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        wrapper.eq(Consumer::getName, "苏昌河");

        // 进行查询
        Consumer consumer = consumerMapper.selectOne(wrapper);

        System.out.println(consumer);
    }

    /**
     * 滚动分页
     */
    @Test
    void findScrollingPage(){
        LambdaEsQueryWrapper<Consumer> lambdaEsQueryWrapper = EsWrappers.lambdaQuery(Consumer.class);
        lambdaEsQueryWrapper.size(10);
        // 必须指定一种排序规则,且排序字段值必须唯一 此处我选择用id进行排序 实际可根据业务场景自由指定,不推荐用创建时间,因为可能会相同
        lambdaEsQueryWrapper.orderByDesc(Consumer::getId);

        SAPageInfo<Consumer> saPageInfo = consumerMapper.searchAfterPage(lambdaEsQueryWrapper, null, 3);

        // 第一页
        System.out.println(saPageInfo);

        // 获取下一页
        List<Object> nextSearchAfter = saPageInfo.getNextSearchAfter();
        SAPageInfo<Consumer> next = consumerMapper.searchAfterPage(lambdaEsQueryWrapper, nextSearchAfter, 10);

        System.out.println(next);

        // 断言
//        Assertions.assertEquals(10, next.getList().size());
    }

    /**
     * 根据条件查询总数
     */
    @Test
    void findCount(){
        // 封装条件
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();
        // 以苏开头
        wrapper.like(Consumer::getName,"苏");
        // 查询数量
        Long count = consumerMapper.selectCount(wrapper);
        log.info("总数:" + count);
    }

    /**
     * 查询过滤 例：一个person中有name，age，address，我们只要name
     */
    @Test
    void findFiltration(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<Consumer>()
                .matchAllQuery()
                .select("name");

        // 进行查询
        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 将查询出的数据排除掉age属性的值
     */
    @Test
    void noSelect(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<Consumer>()
                .matchAllQuery()
                .notSelect("age");

        // 进行查询
        List<Consumer> consumers = consumerMapper.selectList(wrapper);
        consumers.forEach(System.out::println);
    }

    /**
     * 查询以什么什么开头的数据
     */
    @Test
    void findPrefix(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        // 以苏开头
        wrapper.prefixQuery(Consumer::getName, "苏");

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 设置多个name为查询条件 in
     */
    @Test
    void findIn(){

        List<String> names = Arrays.asList("白鹤淮", "苏暮雨");

        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();

        // in 包含
        wrapper.in(Consumer::getName, names);

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 排序查询
     */
    @Test
    void findOrderBy(){

        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();
        // 降序
//        wrapper.orderByDesc(Consumer::getAge);
        // 升序
        wrapper.orderByAsc(Consumer::getAge);

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }

    /**
     * 去重查询
     */
    @Test
    void findDistinct(){
        LambdaEsQueryWrapper<Consumer> wrapper = new LambdaEsQueryWrapper<>();
        // 根据age进行去重 todo 此处去重 为中文会报错
        wrapper.distinct(Consumer::getAge);

        List<Consumer> consumers = consumerMapper.selectList(wrapper);

        consumers.forEach(System.out::println);
    }


}
