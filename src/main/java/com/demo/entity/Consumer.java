package com.demo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.dromara.easyes.annotation.IndexId;
import org.dromara.easyes.annotation.IndexName;


/**
 * 
 * @Project: easy-es-demo
 */
@Data
@NoArgsConstructor
@IndexName("consumer")
public class Consumer {

    public Consumer(String name,Integer age,String remark){
        this.name = name;
        this.age = age;
        this.remark = remark;
    }

    @IndexId
    private String id;

    private String name;

    private Integer age;

    /**
     * 备注 text 类型可分词
     */
    private String remark;

}
