package com.demo.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.dromara.easyes.annotation.HighLight;
import org.dromara.easyes.annotation.IndexField;
import org.dromara.easyes.annotation.IndexId;
import org.dromara.easyes.annotation.IndexName;
import org.dromara.easyes.annotation.rely.Analyzer;

/**
 * 
 */
@Accessors(chain = true)
@Data
@IndexName("user")
public class User {

    @IndexId
    private String id;

    private String name;

    /**
     * 需要被高亮的字段
     */
    @HighLight(preTag = "<b>",postTag = "</b>") // 可以设置高亮的标签
    @IndexField(analyzer = Analyzer.IK_SMART, searchAnalyzer = Analyzer.IK_MAX_WORD) // 设置使用的分词器
    private String address;

}
