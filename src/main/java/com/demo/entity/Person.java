package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dromara.easyes.annotation.IndexId;
import org.dromara.easyes.annotation.IndexName;
import org.dromara.easyes.annotation.rely.IdType;


/**
 * 
 * @Project: easy-es-demo
 * @Date 2023/10/10 下午 11:49
 * explain:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@IndexName("person")
public class Person {

    // 必须有索引否则报错
    @IndexId(type = IdType.UUID)
    private String id;

    private String name;

    private Integer age;

    private String address;

}
