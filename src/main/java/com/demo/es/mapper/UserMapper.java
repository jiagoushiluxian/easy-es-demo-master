package com.demo.es.mapper;

import com.demo.entity.User;
import org.dromara.easyes.core.core.BaseEsMapper;
import org.springframework.stereotype.Repository;

/**
 * 
 */
@Repository
public interface UserMapper extends BaseEsMapper<User> {
}
