package com.demo.es.mapper;

import com.demo.entity.Person;
import org.dromara.easyes.core.core.BaseEsMapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * @Project: easy-es-demo
 * @Date 2023/10/10 下午 11:50
 * explain:
 */
@Repository
public interface PersonMapper extends BaseEsMapper<Person> {
}
