package com.demo.es.mapper;

import com.demo.entity.Consumer;
import org.dromara.easyes.core.core.BaseEsMapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * @Project: easy-es-demo
 * @Date 2023/10/13 上午 10:15
 * explain:
 */
@Repository
public interface ConsumerMapper extends BaseEsMapper<Consumer> {
}
