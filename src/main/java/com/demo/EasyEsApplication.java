package com.demo;

import org.dromara.easyes.starter.register.EsMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @Project: easy-es-demo
 * @Date 2023/10/10 下午 11:48
 * explain:
 */
@EsMapperScan("com.demo.es.mapper")
@SpringBootApplication
public class EasyEsApplication {

    public static void main(String[] args) {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
        SpringApplication.run(EasyEsApplication.class, args);
    }

}
